package com.vgarshyn.test;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.vgarshyn.test.core.ProductCartDispatcher;
import com.vgarshyn.test.core.ProductCartObservable;
import com.vgarshyn.test.core.ProductCartObserver;
import com.vgarshyn.test.fragment.ProductCartFragment;
import com.vgarshyn.test.fragment.ProductGridFragment;
import com.vgarshyn.test.model.ProductItem;

import java.util.List;

/**
 * Main Activity of application.
 * Activity is a controller in our MVC to dispatch fragments.
 * Also our activity implements callback interfaces and that can allow communicate between fragments.
 * This one of approaches to make communication between fragment according to
 * official documentation http://developer.android.com/training/basics/fragments/communicating.html
 *
 * Created by vgarshyn on 14.04.15.
 */
public class MainActivity extends Activity implements ProductCartObservable, ProductCartDispatcher {
    private static final String TAG = MainActivity.class.getSimpleName();

    private ProductCartObserver mProductCartObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            //add ProductGridFragment only at first start
            initFragment();
        } else {
            //if rotation or config changes was occurred then just find fragment by tag
            initObserver(getFragmentManager().findFragmentByTag(ProductGridFragment.TAG));
        }
    }

    /**
     * This method programmatically instantiate main UI fragment
     */
    private void initFragment() {
        ProductGridFragment fragment = ProductGridFragment.newInstance();
        getFragmentManager().beginTransaction().add(R.id.container, fragment, ProductGridFragment.TAG).commit();
        initObserver(fragment);
    }

    /**
     * Passed fragment must implement ProductCartObserver interface.
     * And when cart dataset changed it will be notified via it interface.
     *
     * @param fragment
     */
    private void initObserver(Fragment fragment) {
        try {
            mProductCartObserver = (ProductCartObserver) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(ProductGridFragment.class.getSimpleName() + " must implement interface "
                    + ProductCartObserver.class.getSimpleName());
        }
    }

    @Override
    public void notifyProductRemove(long id) {
        mProductCartObserver.onProductRemove(id);
    }

    @Override
    public void showProductCart(List<ProductItem> items) {
        ProductCartFragment fragment = ProductCartFragment.newInstance(items);
        getFragmentManager().beginTransaction().add(R.id.container, fragment, fragment.toString()).addToBackStack(fragment.toString()).commit();
    }
}
