package com.vgarshyn.test.core;

import com.vgarshyn.test.model.ProductItem;

import java.util.List;

/**
 * This interface display Shopping Cart
 *
 * Created by vgarshyn on 15.04.15.
 */
public interface ProductCartDispatcher {
    /**
     * Invoke to show Shopping Cart with passed products set
     *
     * @param items
     */
    void showProductCart(List<ProductItem> items);

}
