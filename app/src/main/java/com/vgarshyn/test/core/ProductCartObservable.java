package com.vgarshyn.test.core;


/**
 * This interface must implement Shopping Cart
 * for notify observers about product dataset was changed
 *
 * Created by vgarshyn on 15.04.15.
 */
public interface ProductCartObservable {

    /**
     * Notify about product with passed id was removed from Shopping Cart dataset
     *
     * @param id
     */
    void notifyProductRemove(long id);
}
