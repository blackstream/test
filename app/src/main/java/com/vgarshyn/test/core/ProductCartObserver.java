package com.vgarshyn.test.core;

/**
 * This interface must implements observer who
 * want know about changes in Product Cart.
 *
 * Note: ProductCartObserver and ProductCartObservable
 * in this sample is not implement classic programming pattern Observer.
 * Actually in this case it like more Listener.
 *
 * Created by vgarshyn on 15.04.15.
 */
public interface ProductCartObserver {
    /**
     * Called if removing product from cart was occurred.
     *
     * @param id
     */
    void onProductRemove(long id);
}
