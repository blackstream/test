package com.vgarshyn.test.core;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.vgarshyn.test.model.ResponseData;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Loader for data fetching from remote server and map it to our model
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ProductDataLoader extends AsyncTaskLoader<ResponseData> {
    private static final String TAG = ProductDataLoader.class.getSimpleName();

    private static final String SERVER_URL = "https://s3-eu-west-1.amazonaws.com/uploads-eu.hipchat.com/136279/1550669/WTvjXN0c0i7BVHI/products.json";

    public static final int LOADER_ID = 1234;

    private Gson mGsonMapper = new Gson();

    public ProductDataLoader(Context context) {
        super(context);
    }

    @Override
    public ResponseData loadInBackground() {
        ResponseData data = null;

        if (Utils.isNetworkAvailable(getContext())) {
            try {
                String src = fetchDataSource(SERVER_URL);
                if (!TextUtils.isEmpty(src)) {
                    data = mGsonMapper.fromJson(src, ResponseData.class);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "Houston we have a problem! Provide correct dispatch for this situation");
            }
        }

        return data;
    }

    /**
     * Fetching data from passed url via HttpUrlConnection
     *
     * @param serverUrl
     * @return
     * @throws IOException
     */
    private String fetchDataSource(String serverUrl) throws IOException {
        URL url = new URL(serverUrl);
        URLConnection conn = url.openConnection();

        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect();

        InputStream is = httpConn.getInputStream();
        return Utils.processInputStream(is);
    }
}
