package com.vgarshyn.test.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.vgarshyn.test.R;
import com.vgarshyn.test.core.ProductCartObservable;
import com.vgarshyn.test.fragment.adapter.CartItemsAdapter;
import com.vgarshyn.test.model.ProductItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This fragment represent Shopping Cart.
 * SwipeListView allows remove items from cart by gesture swipe-to-left.
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ProductCartFragment extends Fragment {
    private static final String TAG = ProductCartFragment.class.getSimpleName();

    private static final String KEY_ARG_ITEMS = "arg.items";

    private CartItemsAdapter mAdapter;
    private TextView mAmountTextView;
    private SwipeListView mListView;
    private ProductCartObservable mObservableDelegate;

    /**
     * Instantiate fragment with passed dataset.
     * Data will stored in arguments of fragment.
     *
     * @param items
     * @return
     */
    public static ProductCartFragment newInstance(List<ProductItem> items) {
        ProductCartFragment fragment = new ProductCartFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_ARG_ITEMS, new ArrayList<ProductItem>(items));
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Default adapter's observer. Called if dataset in adapter was changed
     */
    private DataSetObserver mChoiceChangeObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            float amount = getTotalAmount();
            mAmountTextView.setText(getString(R.string.text_total_amount, amount));
        }
    };

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mObservableDelegate = (ProductCartObservable) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.getClass().getSimpleName() + " must implement interface "
                    + ProductCartObservable.class.getSimpleName());
        }
        mAdapter = new CartItemsAdapter();
        mAdapter.registerDataSetObserver(mChoiceChangeObserver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_cart, container, false);
        mListView = (SwipeListView) layout.findViewById(R.id.list);
        mAmountTextView = (TextView) layout.findViewById(R.id.textview_amount);
        return layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter.updateDataset(getArgumentsItems());
        mListView.setAdapter(mAdapter);
        mListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onDismiss(int reverseSortedPositions[]) {
                if (reverseSortedPositions != null && reverseSortedPositions.length > 0) {
                    long id = mAdapter.removeItem(reverseSortedPositions[0]);
                    if (id > -1) {
                        mObservableDelegate.notifyProductRemove(id);
                    }
                }
            }
        });

    }

    @Override
    public void onDetach() {
        mAdapter.unregisterDataSetObserver(mChoiceChangeObserver);
        super.onDetach();
    }

    /**
     * This dirty variant to pass dataset between fragments, but it worked.
     * Maybe more correctly is use Parceble, but at this sample it no matter.
     *
     * @return
     */
    private List<ProductItem> getArgumentsItems() {
        if (getArguments() != null && getArguments().containsKey(KEY_ARG_ITEMS)) {
            return (ArrayList) getArguments().getSerializable(KEY_ARG_ITEMS);
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Calculate total amount of items from adapter
     * @return
     */
    private float getTotalAmount() {
        float amount = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            amount += mAdapter.getItem(i).getAmount();
        }
        return amount;
    }
}
