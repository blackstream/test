package com.vgarshyn.test.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;

import com.readystatesoftware.viewbadger.BadgeView;
import com.vgarshyn.test.R;
import com.vgarshyn.test.core.ProductCartDispatcher;
import com.vgarshyn.test.core.ProductCartObserver;
import com.vgarshyn.test.core.ProductDataLoader;
import com.vgarshyn.test.fragment.adapter.ProductsAdapter;
import com.vgarshyn.test.model.ProductItem;
import com.vgarshyn.test.model.ResponseData;

import java.util.ArrayList;
import java.util.List;

/**
 * This fragment represent GridView with Product items and
 * also it dispatch Loader to fetch data from remote server.
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ProductGridFragment extends Fragment implements ProductCartObserver {
    public static final String TAG = ProductGridFragment.class.getSimpleName();

    private GridView mGridView;
    private ImageButton mBtnCart;
    private BadgeView mBadgeView;
    private ProductsAdapter mAdapter;
    private ProductCartDispatcher mProductCartDispatcher;

    public static ProductGridFragment newInstance() {
        return new ProductGridFragment();
    }

    public ProductGridFragment() {
        super();
    }

    /**
     * Callback to dispatch ProductDataLoader work
     */
    private LoaderManager.LoaderCallbacks<ResponseData> mLoaderCallback = new LoaderManager.LoaderCallbacks<ResponseData>() {

        @Override
        public Loader<ResponseData> onCreateLoader(int id, Bundle args) {
            return new ProductDataLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<ResponseData> loader, ResponseData data) {
            if (data != null) {
                showProducts(data.getProducts());
            } else {

            }
        }

        @Override
        public void onLoaderReset(Loader<ResponseData> loader) {

        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            //Getting ProductCartDispatcher from MainActivity
            mProductCartDispatcher = (ProductCartDispatcher) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.getClass().getSimpleName() + " must implement interface "
                    + ProductCartDispatcher.class.getSimpleName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_products, container, false);
        mGridView = (GridView) layout.findViewById(R.id.products_grid);
        mBtnCart = (ImageButton) layout.findViewById(R.id.btn_cart);
        mBadgeView = new BadgeView(getActivity(), mBtnCart);
        return layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ProductsAdapter();
        mGridView.setAdapter(mAdapter);
        mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateCartButtonState();
            }
        });
        mBtnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProductCartDispatcher.showProductCart(getSelectedProducts());
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(ProductDataLoader.LOADER_ID, null, mLoaderCallback).forceLoad();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartButtonState();
    }

    /**
     * Update dataset in adapter and adapter invoke GridView invalidating
     *
     * @param products
     */
    private void showProducts(List<ProductItem> products) {
        mAdapter.updateDataset(products);
    }

    /**
     * Update shopping cart button and Bage Counter
     */
    private void updateCartButtonState() {
        int count = mGridView.getCheckedItemCount();
        if (count > 0) {
            mBadgeView.setText(String.valueOf(count));
            mBadgeView.show();
        } else {
            mBadgeView.hide();
        }
        mBtnCart.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * Gathering checked items into List of ProductItems
     *
     * @return
     */
    private List<ProductItem> getSelectedProducts() {
        List<ProductItem> items = new ArrayList<ProductItem>();
        SparseBooleanArray checkedItemPositions = mGridView.getCheckedItemPositions();
        for (int i = 0; i < mAdapter.getCount(); i++) {
            if (checkedItemPositions.get(i)) {
                items.add(mAdapter.getItem(i));
            }
        }

        return items;
    }

    /**
     * Deselect product which was removed in Shopping Cart.
     *
     * @param id
     */
    @Override
    public void onProductRemove(long id) {
        SparseBooleanArray checkedItemPositions = mGridView.getCheckedItemPositions();
        for (int position = 0; position < mAdapter.getCount(); position++) {
            if (checkedItemPositions.get(position) && mAdapter.getItemId(position) == id) {
                mGridView.setItemChecked(position, false);
                updateCartButtonState();
                break;
            }
        }
    }
}
