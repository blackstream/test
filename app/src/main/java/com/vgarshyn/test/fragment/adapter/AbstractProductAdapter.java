package com.vgarshyn.test.fragment.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.vgarshyn.test.model.ProductItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This adapter contains common logic to display
 * dataset of ProductItems for GridView or ListView.
 * Inheritors must implement only 2 methods:
 * getItemLayoutId and instantiateViewHolder
 *
 * Created by vgarshyn on 15.04.15.
 */
public abstract class AbstractProductAdapter extends BaseAdapter {
    private static final String TAG = AbstractProductAdapter.class.getSimpleName();

    protected List<ProductItem> mItems = new ArrayList<ProductItem>(0);

    /**
     * Set new list of product items to adapter
     *
     * @param dataset
     */
    public void updateDataset(List<ProductItem> dataset) {
        if (dataset != null && !Collections.EMPTY_LIST.equals(dataset)) {
            mItems = dataset;
            notifyDataSetChanged();
        }
    }

    /**
     * Remove item at position and return id of removed ProductItem
     *
     * @param position
     * @return
     */
    public long removeItem(int position) {
        long id = -1;
        if (position >= 0 && position < getCount()) {
            id = getItemId(position);
            mItems.remove(position);
            notifyDataSetChanged();
        } else {
            Log.e(TAG, "Attempt to remove item at out of bounds");
        }
        return id;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public ProductItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(getItemLayoutId(), parent, false);
            holder = instantiateViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (IViewHolder) convertView.getTag();
        }
        ProductItem item = getItem(position);
        holder.display(item);
        return convertView;
    }

    /**
     * This method must return item's layout id
     *
     * @return
     */
    protected abstract int getItemLayoutId();

    /**
     * This method must return instance of ViewHolder for passed convertView
     *
     * @param convertView
     * @return
     */
    protected abstract IViewHolder instantiateViewHolder(View convertView);

    /**
     * Interface for describe ViewHolder behavior and abstract it from particular instances
     */
    protected interface IViewHolder {
        void display(ProductItem data);
    }

}
