package com.vgarshyn.test.fragment.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vgarshyn.test.R;
import com.vgarshyn.test.model.ProductItem;

/**
 * Adapter for ListView in ProductCartFragment
 *
 * Created by vgarshyn on 15.04.15.
 */
public class CartItemsAdapter extends AbstractProductAdapter {

    @Override
    protected int getItemLayoutId() {
        return R.layout.item_list_row;
    }

    @Override
    protected IViewHolder instantiateViewHolder(View convertView) {
        return new ViewHolder(convertView);
    }

    private class ViewHolder implements IViewHolder{
        ImageView image;
        TextView title;
        TextView price;

        ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            price = (TextView) view.findViewById(R.id.price);
        }

        public void display(ProductItem item) {
            title.setText(item.getTitle());
            price.setText(item.getPrice());
            Picasso.with(image.getContext()).load(item.getSmallImageUrl()).fit().into(image);
        }
    }
}
