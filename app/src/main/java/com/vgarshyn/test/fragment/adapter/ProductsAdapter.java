package com.vgarshyn.test.fragment.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vgarshyn.test.R;
import com.vgarshyn.test.model.ProductItem;

/**
 * Adapter for GridView
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ProductsAdapter extends AbstractProductAdapter {
    private static final String TAG = ProductsAdapter.class.getSimpleName();

    public ProductsAdapter() {
        super();
    }

    @Override
    protected int getItemLayoutId() {
        return R.layout.item_grid_view;
    }

    @Override
    protected IViewHolder instantiateViewHolder(View convertView) {
        return new ViewHolder(convertView);
    }

    /**
     * In this case ViewHolder very similar with CardItemsAdapter$ViewHolder
     * but I decide not generify it. This allow making UI more flexible in future.
     */
    private class ViewHolder implements IViewHolder{
        ImageView image;
        TextView title;
        TextView price;

        ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            price = (TextView) view.findViewById(R.id.price);
        }

        public void display(ProductItem item) {
            title.setText(item.getTitle());
            price.setText(item.getPrice());
            Picasso.with(image.getContext()).load(item.getSmallImageUrl()).fit().placeholder(R.drawable.image_placeholder).into(image);
        }
    }
}
