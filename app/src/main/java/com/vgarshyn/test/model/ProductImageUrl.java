package com.vgarshyn.test.model;

import com.google.gson.annotations.SerializedName;

/**
 * This model allows extract picture's url for product
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ProductImageUrl {

    @SerializedName("small")
    String small;

    @SerializedName("large")
    String large;

    public String getSmall() {
        return small;
    }

    public String getLarge() {
        return large;
    }
}
