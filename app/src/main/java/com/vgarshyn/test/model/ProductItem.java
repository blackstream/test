package com.vgarshyn.test.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 *  This model class describe product information from JSON response.
 *  For simplify I mapped only necessary fields for sample.
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ProductItem {

    @SerializedName("id")
    long id;

    @SerializedName("title")
    String title;

    @SerializedName("productImages")
    List<ProductImageUrl> imageUrls;

    @SerializedName("priceAmount")
    float amount;

    @SerializedName("price")
    String price;


    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSmallImageUrl() {
        return imageUrls.get(0).getSmall();
    }

    public float getAmount() {
        return amount;
    }

    public String getPrice() {
        return price;
    }

}
