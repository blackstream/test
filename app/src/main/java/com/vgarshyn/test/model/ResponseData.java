package com.vgarshyn.test.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Root model class that describe our JSON response
 *
 * Created by vgarshyn on 14.04.15.
 */
public class ResponseData {

    @SerializedName("errid")
    String errorId;

    @SerializedName("response")
    List<ProductItem> products;

    public List<ProductItem> getProducts() {
        return products;
    }
}
